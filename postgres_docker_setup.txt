1. postgresql image ziehen:

	docker pull postgres


2. Docker-Container starten:

	docker run --rm --name db-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data  postgres

	--> '-e POSTGRES_PASSWORD=docker' setzt das Passwort auf 'docker'
	--> '-v $HOME/docker/volumes/postgres:/var/lib/postgresql/data' sorgt daf�r, dass die db im Ordner '$HOME/docker/volumes/postgres' gespeichert wird.

	--> default username: postgres
	--> default db name: postgres

3. Falls die Datenbank neu mit dem Backend verbunden wird muss zuerst eine Migration durchgef�hrt werden
--> python3 manage.py migrate


#############
#ANMERKUNGEN#
#############

Damit das funktioniert muss das python modul psycopg2 dort installiert sein, wo Django l�uft!!! Wichtig!! Deswegen ging es bei mir nicht!!!
	--> pip3 install psycopg2-binary



Damit die Datenbank nicht verloren geht wenn der Container stirbt muss sie in einem eigenen Ordern gespeichert werden. Das macht '-v $HOME/docker/volumes/postgres:/var/lib/postgresql/data'
--> Damit das funktioniert muss der Ordner nat�rlich auch angelegt sein!


(Entsprechend m�ssen die Eintr�ge f�r DATABASES in settings.py angepasst werden! -- das ist in der aktuellen Version schon so gemacht. Muss man sich nicht drum k�mmern.)


