const urlFor = (endpoint) => {
 var host = window.location.hostname;
 return `http://${host}:8080/${endpoint}`;
};

export default urlFor;