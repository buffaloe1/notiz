from django.db import models

class Note(models.Model):
    name = models.CharField(max_length=40)
    text = models.CharField(max_length=400)

    def __str__(self):
        return self.text
